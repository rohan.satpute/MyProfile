**Rohan Satpute**

**Project Manager** | Pune - 411057

***Agile Product & Program Management, Quality Management***

_+91 9820 156842 rohan.satpute@gmail.com www.linkedin.com/in/satputerohan_

Professional Summary
====================
•	Digital Agile Program Management: Highly Self-Motivated, Passionate seasoned Professional with 13+ years of premium experience in Agile Product, Program & Quality Management, DevOps, Test Automation & Delivering multi-million-dollar projects for multiple Cross-Functional Teams across versatile Geographically Distributed Customer Engagements.
•	Delivery Excellence: Keen Planner, Strategist & Implementer with demonstrated success in End-to-End Agile Project and Program Management with relevant accredited recognized Certifications, Stakeholder & People Management including Project Financials & Governance, Project Risks & Ensuring On-Time deliveries within project budget & Manage Flow of Value leading to High Customer Satisfaction.
•	SAFe, Scrum, DevOps & Test Automation Expert: Being certified SAFe Agilist & Scrum Master, Responsible for Successful Agile Scrum/Kanban Framework Implementation across multiple Complex Products, Value Streams, Business Lines, Methodology Compliance and Manage Resolutions, Employing Continuous Delivery Pipeline, Continuous Testing with Automation of User Stories. Expertise in Quality Management, Control & Assurance, Project Life Cycle, DevOps, Test Technologies across Web, Mobile, & Hybrid Application Testing, TDD, Disruptive Testing & BDD Test Implementation. Hands-On Experience in DevOps tooling, Processes & Improvement, Cloud Computing Technology Services, Good Understanding of CI-CD pipeline, working on Virtual Private Cloud Google Cloud Platform, VM Setups, Exposure to Continuous Testing & Test Automation on multiple applications & platforms. 
•	Strong Leadership & Communication Skills: Good Mentor with Strong People Management, Effective Interpersonal Skills, Listening, Oral, Written and Presentation Skills, Relationship-Building and Negotiation Skills, Multi-Tasking, Analytical & Design Thinking Problem-Solving skills. Address Critical Bottlenecks, Escalate Impediments, help ensure Value Delivery and help Drive Relentless improvement, Establish & Communicate Iterations plans, the Proven track record of Cross-Functional Team Collaboration, Implement Retrospectives.
•	Innovation & Strategic Leadership: Effectively Managed Large programs with Innovative and Strategic ideas, Provision Technical Leadership and Devise Test Strategy for Testing Organization, Define R&R for Team Members, Develop Solutions, Ensure Strategic and Execution Alignment to Improve the Flow of Value through Value Streams using Continuous Delivery Pipeline and DevOps. Conceptualized, Innovated & Managed Test Automation Framework for Various IT, Telecom Products & Projects enabling Continuous Integration. Managed Third Party Outsourced Projects as well.
•	Ground-Breaking Initiatives: Project Cost Optimizations with Automation, Continuous Automation Testing reducing Production Defects, Cost-Benefit Initiatives, Lowering Operational Cost, AT&T 20-20 Vision of Digitization, Test Management & Data Optimization. Managing Quality Gates & Publish Dashboards Data, Onshore-Offshore Model, Engagement with Multiple Stack holders including Client and provides Customer-Facing Organizations with necessary technical product support in making Presentations, Client Visits & Live Demonstrations, POC's, Video-Conference meetings, along with Status Reporting to Management.
•	Project Governance: Project financials, T&M/Fixed Bid/Price Contract Management, Bill-ability Forecasting, Resource Utilization, Audits, Quality & Productivity Improvement, Metrics Reporting, E-SAT, C-SAT, SLA, Stakeholders Management, Cost Optimization, Re-scaling, Resizing, etc.

Awards/Rewards

	Best Team Award (2017-2018) 
	Best Team Award, Pune Location, TechM Annual Day "Ekatvam" (2016-17)
	Best Team Award (2014-2015)
	Pat On Back (Mar 2015)
	Most Valuable Player Award 2009
	Nominated "Project Manager of the Year 2015-2016", Pune Location, TechM Annual Day "Ekatvam" (2015-2016)
	Standing Ovation Annual Award (2016-17)
	Associate of the Month (Mar 2016)
	Most Valuable Player Award 2005

Experience

Tech Mahindra Ltd. | Project Manager Sept 2013 to till date

Working Upon: Program Management, Project-based, SAFe/Agile-Scrum, Kanban, DevOps
Account/Clients: MyATT Portal/Mobile App, AT&T, US

Roles/Responsibilities:
•	Agile Program Management: Provide Strong leadership and oversight on various projects of a complex nature & ensure timely completion of project deadlines and milestones with Quality. Successfully Managed & Responsible for End-to-End Agile Delivery of multiple complex projects (10-25+) MyATT Portal/Mobile to AT&T with Large Test Team size (~100+) as per defined Release/Sprint Calendar managing multiple critical projects attributes like Web/Mobile System & Integration Testing, Cross-Browsers/Cross-Device Testing using real/stub data & Real Device/Simulators, Accessibility Test (CATO), Reporting & Chat Testing, Continuous Testing & Test Automation enabling CI/CD pipeline, UAT, PVT & Production Support. Produce progress reports/dashboards, issues/risks & communicates to senior management regarding the status of specific project deliverable regularly.
•	SAFe/DevOps Transformation: Highly Motivated Project manager cum Lean-Agile Leader to deliver the overall objectives of project/program. Being SAFe Agilist, instrumental in the successful adoption of SAFe in MyATT, empower and help Agile teams build better systems by learning, exhibiting, teaching and coaching SAFe’s Lean-Agile principles and practices, build a Continuous Delivery Pipeline, DevOps culture and coordinate large solutions. Developed Strong understanding of DevOps Concepts, workflows, Best practices & ability to convert DevOps solutions into people adoption. Hands-on knowledge of DevOps tooling, CI-CD & Continuous Testing pipeline with active participation in implementation and adherence to CI-CD-CT practices, including  Build Automation, Unit/Contract/Smoke Testing,  Sonar Quality Gates, Test Automation, Functional/Non-Functional Testing, System Integrations & Regression Testing, Accessibility Testing, UAT &  Production Testing using Tools like Gitlab, Maven, Jenkins, Sonar, PACT, Rest Assured, Docker, Nexus, Ansible, Automatics (Java-Selenium-Appium based Test Automation tool), etc. Enabling Inspect & Adapt culture & Learning DevSecOps.
•	Agile Mentor/Coach: Being an advocate for Agile Culture, own the implementation of Agile tools and practices within a business area and communicate the progress to leadership. Experienced in Agile Project, Portfolio or Process Management discipline with a focus on leading teams to deliver technology focused projects. By keeping control of the various agile parameters like Agile Dashboards, velocity, burn charts, release reports, etc. ensure correct balance project deliveries and implementing Agile best practices. Ensuring healthy application of agile practices and providing assistance to teams and programs when needed like Mentor Team on Agile Capability services via training, topic-specific workshops, embedded coaching, spot coaching, team startup etc. Assess teams and ongoing Agile processes to derive coaching actions towards increasing maturity levels by fetching actions from Agile metrics & instill a culture of continuous improvement. Enable close cooperation across all Scrum/Management Roles and functions & Arrange daily stand-up meetings, facilitate meetings, schedule meetings, demo and decision-making processes to ensure quick inspection and proper use of adaptation process. Managing remote and geographically distributed agile teams. 
•	Quality Management & Test Automation: Performing multiple roles as a Test Architect for SAFe ART, Test Manager & Automation Expert for DevOps & Waterfall Teams. Responsible for driving adoption of Quality Engineering services like Testing Services, Testing and Environment automation, Development Collaboration, Continuous Integration, Continuous Testing, Various Testing Techniques and Test automation covering all key practice areas including Mobile, Web, and API testing. Lead release level quality & interacts with multiple technology staff to identify the scope of the project and deliver services. Implementation of BDD/TDD practices for SAFe Framework using Cucumber. Foster strategic thinking and continuous learning to help customer continue to lead a technology company providing differentiating high-quality features and services.
•	Financials & Governance: Develop and carry out detailed program management plan, status reporting and secure signoffs from sponsors. Identifies relationship needs, explores relationship opportunities and formulate action plans. Contributes to the definition of business case & program charter alignment in order to build and strengthen client relationships.
•	Resource/People Management: Determine & track people, hardware and software infrastructure needs such as skillset, technology, machine spares, licenses & accesses in order to ensure that the program has the required and appropriate resources. Enables ongoing competency development of the team. Implements coaching and mentoring frameworks within the program and personally spends adequate time on coaching and mentoring the next level of employees. Communicates and ensure adherence to organization policies and processes in order to ensure engaged employees to deliver business results.

Roamware, (Now Mobileum) | Quality Manager, Scrum Master Mar 2005 to Sep 2013

Worked Upon: Product-Based, Quality Manager, Test Architect, Agile-Scrum, Waterfall
Account/Clients: Server customer across Globe for Business Line Products

Saitech Data Systems (India) Pvt. Ltd.* | Software Engineer Dec 2003 to Feb 2005
*This was a startup which has been shut down in 2008 so verification check isn’t feasible. Tech Mahindra had done a verification check and the evidence of the same can be provided.

Professional Skills

	Program Management
	Stakeholder Management
	Test Automation 
	Quality Management
	CI CD Pipeline 
	Agile-Scrum, Kanban 
	Scaled Agile, DevOps
	Team Building
	Test Architecture

IT Skills

Agile Transformation Agile Principles, Scrum Framework & Values, Scrum Roles, Scrum Ceremonies, Scrum Artefacts, Estimations, Velocity, Scrum of Scrum, & Distributed Scrum implementation, TDD, BDD, DevOps, Continuous Integration & Improvement, etc.
Test Automation Automation Framework development for multiple Roamware products & features, Devise Own Test Tool (iTester) & Customize other Automation Tools as per Business needs, ensured value add by supporting Web & Device Automation for MyAT&T Applications, Product Bench-marking, QPM, Functional & Non-Functional Testing, Mobile, Native, Web, Hybrid App Testing, innovative solutions & creative thinking.
Digitization Delivery Digital Project Manager, Digital Architect, Digital Consultant, Digital Tester, Android Overview, RWD, IoT, Mobility, Cloud, Digitization in Banking, Insurance, Utilities, Telecom, Payments, Media & Entertainment
Agile & Testing Tools AgileCraft, Rally, VersionOne, JIRA, Ideaboardz, RQM, Cucumber, QTP, QC, NetHawk EAST, LOADRUNNER, Automatics, SELENIUM, Appium, TektronicsK1297(PA), Wireshark, iTester (Roamware Testing Tool), Rational Pure Coverage & Purify, Target Process
DevOps Concepts & Process, Virtual Private Cloud VPC, Google Cloud Platform GCP, Chef, Puppet, Docker, Ansible, Salt Stack, Jenkins, GIT, Maven, Nagios, Cucumber, Virtualization & Containerization, Familiarity with AWS & Azure, SONARQUBE, IaaS, PaaS, SaaS
Core Domain Knowledge GSM, GPRS, SS7, IN, CAMEL, MAP, TCAP, ISUP, GTP, Diameter, Radius, VAS, OTA, TCP/IP, UDP, SNMP, WAP, RTSP, SIP, RTP, FTP, SMTP, DPI, API, CTI, DTMF, IVR, DK cards, T1-E1, ISDN, Telecom
Application/ Scripting MS-Project, MS-Word, MS-Excel, MS-Visio, MS-PowerPoint, Toad, CVS, VSS, Bugzilla, Unix, Linux, Solaris, Windows; Oracle/SQL; Perl/Shell scripting, XML, HTML, Web Services, Apache Tomcat, Java, SQL

Certifications

•	SAFe 4 Certified Agilist SA (Scaled Agile)
•	Certified Scrum Master CSM (Scrum Alliance)
•	PRINCE2 AGILE & PRINCE2 (APMG)
•	ISTQB Foundation Level Certification For more detail, check LinkedIn Profile

Education

•	Secured 65% in BE (E&TC) (June 2003) from Govt. College of Engineering, Amravati.
•	Secured 82% in HSC (June 1999) from Maharashtra Board.
•	Secured 75% in SSC (June 1997) from Maharashtra Board.

Achievements

•	Offshore Ownership for ensuring, driving rescaling & fulfillment of current TechM-MyATT Engagement to Scaled Agile & Agile-Scrum/Kanban, DevOps Tools/Skills, Test Automation using Selenium/Appium, API Testing using PACT/Rest Assured, Building Skillset for SDET (Software Development Engineer for Testing) & ATE (Automation Test Engineers) roles, etc., effectively working as an Lean-Agile Leader, Change Agent for Cultural Shift.
•	For MyATT Services Account with 130+ million subscriber base, foster revenues worth 2.7 million USD, expanded TechM Client Base by 400% with consistency in Delivery Excellence, Accomplished Testing Engagement growth from 50 to 200 headcounts across multiple geographies, reduced team attrition rate with improved E-SAT.
•	Achieved Incremental C-SAT ratings currently stationed at 8.6/10, the increment of 0.3 YOY, Awarded Supplier of the Year. 
•	Successfully Renewed, Performed Transition of MyATT Engagement contract from T&M to Fixed Bid/Price with release work package size ranging from 25K to 1.75L weighted hours consists of multiple critical & complex individual Projects counts ranging from 25 to 80, per production release across 10 different modules. 
•	As Agile Expert, Instrumental in Agile Implement & Transformation for Program
•	Lead QA for Roaming Flagship Product NTR with a Customer base of around 200+ Mobile Operators across the globe for 5+ years, with High Quality & Timely Deliverable to End Customers in order to ensure CSAT along with ESAT & Achieving Quarterly Revenue Targets.
•	Devised Test Automation Framework Suites for various Telecom VAS Products, Web Services, E2E Complete Product Deployment Cycle, Product Benchmarking, etc. based Data-Driven, Keyword Driven, Hybrid with Test Coverage Functional as well as Non-Functional Parameters as Performance, Load, Stress, etc. Key Contributor to Test Automation Tool development like iTester@Roamware, IRDBUploader@Roamware, Automatics@TechM, etc. Also instrumental in Roamware GUI Automation through the third party CreshTech. This initiative turned up to a key contributor for ensuring Continuous Automation Testing & Deployment, Reduced Time Market, Competitive Advantage, Staying Relevant, etc. Devise Continuous Testing Hub supporting Frequent & Fast Releases. Achieving Project Cost-Benefit Initiatives, thus practiced TechM’s RISE culture.
•	Elevation of 2 projects from CMM Level L2 to CMM Level L4 via Quantitative Project Management by identifying 2 Improvement Goals Objectives “Field Defect Density” & “Cancel Defect Density”, thus achieving dual's Objectives, Customer Goals like AQI improvements as well as TechM Business Unit Goals.
•	Performed Product Deployment, Test, Performance Benchmarking & User Acceptance Test at various Tier-1 Telecom Operator Sites. Also, Performed Product Performance Tuning & Configuration Audit for VAS.
•	Practice Situational Leadership, Directed a team of 100+ through all stages of deadline-driven multiple complex projects deliveries
•	Implemented Innovations like Offline Testing Techniques, Multiple Browsers testing, automate as we GO, etc.
•	Adroitly Simplified & Implemented Generic Country/Network Data updates for various products, which was resulting in reducing Product wise revenue leakages.
•	Implemented & Baselined Project Execution Process in Agile for newly allotted products, as NTR was selected for the Best Executed Product under Yearly Project Execution Analysis within Roamware in 2010.
